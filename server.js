"use strict";

const appRoot = require('app-root-path');
const properties = require('properties-reader')(appRoot + '/properties.ini')
const express = require('express')
const fs = require('fs')
process.env.NODE_ENV = "development"
const app = express()
app.set('secureMode', 'http');

var server = require('http').createServer(app);
const serverPort = process.env.PORT || properties.get('DEV.SERVER.PORT');
require(appRoot + "/helpers/middleware_helper")(app)
require(appRoot + "/helpers/route_helper")(app)
server.listen(serverPort, function () {
    console.log("Custom - Starting in - " + process.env.NODE_ENV + " mode",
    "Custom - App listening at - " + server.address().port + " in ",
    app.get('secureMode')
    );
})


