"use strict";
const interceptor = require('express-interceptor');

const apiFinalInterceptor = interceptor(function (req, res) {
    const appRoot = require('app-root-path');
    const properties = require('properties-reader')(appRoot + '/properties.ini')
    const APPCONSTANTS = require(appRoot + "/resources/constants");
    const dontParseStatuses = [
        APPCONSTANTS.HTTP_STATUS.NOT_MODIFIED.CODE
    ]
    return {
        isInterceptable: function () {
            return (dontParseStatuses.indexOf(res.statusCode) == -1)
        },
        intercept: function (body, send) {
            
            var responseData = { status: { code: res.statusCode }, count:1000 }
            var resIncomingBody = JSON.parse(body)
            
            switch (res.statusCode) {
                case APPCONSTANTS.HTTP_STATUS.BAD_REQUEST.CODE:
                case APPCONSTANTS.HTTP_STATUS.NOT_AUTHENTICATED.CODE:
                case APPCONSTANTS.HTTP_STATUS.VALIDATION_ERROR.CODE:
                case APPCONSTANTS.HTTP_STATUS.DB_ERROR.CODE:
                case APPCONSTANTS.HTTP_STATUS.NO_DATA.CODE:
                    responseData.status.message = "Error"
                    responseData.error = resIncomingBody
                    break;

                case APPCONSTANTS.HTTP_STATUS.NOT_FOUND.CODE:
                case APPCONSTANTS.HTTP_STATUS.INTERNAL_SERVER.CODE:
                    responseData = resIncomingBody;
                    break;

                default:
                    
                    responseData.status.message = "Success"
                    responseData.data = resIncomingBody
                    if(resIncomingBody.response){
                        responseData.result = resIncomingBody.response
                    }
                    break;
            }

            // returns updated api result
            send(JSON.stringify(responseData))
        }
    };
})

module.exports = apiFinalInterceptor