
"use strict";
const authParams = new Object();

const entryHelper = function (req, res, next) {
    const appRoot = require('app-root-path');
    const constants = require(appRoot + "/resources/constants");
    const properties = require('properties-reader')(appRoot + '/properties.ini')


    authParams.reqData = {
        method: req.method,
        originalUrl: req.originalUrl
    }
    req.properties = properties
    req.appRoot = appRoot
    req.APPCONSTANTS = constants

    next()
}

module.exports = entryHelper;