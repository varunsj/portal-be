"use strict";
const routes = function (app) {
    const appRoot = require('app-root-path');
    // const logger = require(appRoot + '/helpers/library/wintson')

    // entry middleware
    app.use(require(appRoot + '/helpers/entry_helper'))

    // final interceptor middleware
    app.use(require(appRoot + '/helpers/intercept_helper'));
    app.use('/v1', require(appRoot + "/apps/v1/routes"));
    // Catch 404 and forward to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    console.log('12333333333333333333333333')

    // Error handler
    app.use(function (err, req, res, next) {
        console.log(req.app.get('env'), '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        // Set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        var errStatus = err.status || 500;
        console.log(`${errStatus} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
        // Render the error page

        res.status(errStatus)
            .send({ status: { code: errStatus, message: 'Error' }, error: { message: err.message } });
    });
}

module.exports = routes