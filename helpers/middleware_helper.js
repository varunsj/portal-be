"use strict";
const middlewares = function (app) {
    const appRoot = require('app-root-path');
    const bodyParser = require('body-parser')
    const cors = require('cors')
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(cors({
        credentials: true,
    }));
    app.options('*', cors());
}

module.exports = middlewares;