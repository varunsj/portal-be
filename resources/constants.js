"use strict";
module.exports = {
  HTTP_STATUS: {
    SUCCESS: { CODE: 200, MESSAGE: "Success" },
    CREATED: { CODE: 201, MESSAGE: "Created" },
    UPDATED: { CODE: 202, MESSAGE: "Updated" },
    DELETED: { CODE: 203, MESSAGE: "Deleted" },
    NO_CONTENT: { CODE: 204, MESSAGE: "No content" },
    API_SUCCESS: { CODE: 200, MESSAGE: "API Responded" },
    NOT_MODIFIED: { CODE: 304, MESSAGE: "Not modified" },
    BAD_REQUEST: { CODE: 400, MESSAGE: "Bad request" },
    NOT_AUTHENTICATED: { CODE: 401, MESSAGE: "Not authenticated" },
    NOT_FOUND: { CODE: 404, MESSAGE: "Not found" },
    MAIL_SEND_ERROR: { CODE: 420, MESSAGE: "Mail send error" },
    THIRD_PARTY_API_ERROR: { CODE: 421, MESSAGE: "Third party API error" },
    VALIDATION_ERROR: { CODE: 422, MESSAGE: "Validation error" },
    NO_DATA: { CODE: 423, MESSAGE: "No data" },
    INTERNAL_SERVER: { CODE: 500, MESSAGE: "Internal server" },
    DB_ERROR: { CODE: 503, MESSAGE: "DB error" }
  },
}

