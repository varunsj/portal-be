"use strict";
const appRoot = require('app-root-path') + "/apps/v1/"
const router = require('express').Router();
const dashboardcontroller = require(appRoot + "dashboard/controller")
router.post('/dashboard/data', dashboardcontroller.dashboardData);
router.post('/login_auth', dashboardcontroller.authenticate);
router.post('/dashboard/get/UrlDataSource', dashboardcontroller.getDataPerGetRequest)
module.exports = router;