const appRoot = require('app-root-path');
const subtasks = [
    { "taskID": 31, "taskName": 'Development Task 1', "startDate": new Date('02/17/2017'), "endDate": new Date('02/19/2017'), "duration": 3, "progress": '50', "priority": 'Low', "approved": true },
    { "taskID": 32, "taskName": 'Development Task 2', "startDate": new Date('02/17/2017'), "endDate": new Date('02/19/2017'), "duration": 3, "progress": '50', "priority": 'Normal', "approved": false },
    { "taskID": 33, "taskName": 'Testing', "startDate": new Date('02/20/2017'), "endDate": new Date('02/21/2017'), "duration": 2, "progress": '0', "priority": 'Critical', "approved": true },
    { "taskID": 34, "taskName": 'Bug fix', "startDate": new Date('02/24/2017'), "endDate": new Date('02/25/2017'), "duration": 2, "progress": '0', "priority": 'High', "approved": false },
    { "taskID": 35, "taskName": 'Customer review meeting', "startDate": new Date('02/26/2017'), "endDate": new Date('02/27/2017'), "duration": 2, "progress": '0', "priority": 'Normal', "approved": true },
    { "taskID": 36, "taskName": 'Phase 3 complete', "startDate": new Date('02/27/2017'), "endDate": new Date('02/27/2017'), "duration": 0, "priority": 'Critical', "approved": false },
]
exports.getDataPerGetRequest = async function (things) {
    let jsonData = await getData()
    if(jsonData){
        let array = JSON.parse(jsonData);
        let limit = 30
        limit = limit + Number(things.params.skip)
        retval = array.slice(things.skip, limit)
        // retval.total_count = array.length
        // retval.data = array.slice(things._skip, things.limit)
        // for (let index = 0; index < retval.data.length; index++) {
        //     let element = retval.data[index];
        //     element.subtasks = subtasks
        // }
    }
    return retval
}
exports.getTxtData = async function (things) {
    let retval = {
        array : []
    }
    try {
        let jsonData = await getData()
        if(jsonData){
            let array = JSON.parse(jsonData);
            retval.total_count = array.length
            retval.data = array.slice(things._skip, things.limit)
            // for (let index = 0; index < retval.data.length; index++) {
            //     let element = retval.data[index];
            //     element.subtasks = subtasks
            // }
        }
        
        return retval
    } catch (e) {
        console.log(e)
        return retval
    }
}
exports.getAllElements = async function (things = null) {
    let retval = []
    try {
        let jsonData = await getData()
        if(jsonData){
            retval = JSON.parse(jsonData);
        }
        return retval
    } catch (e) {
        console.log(e)
        return retval
    }
}
async function getData() {
    const file = require('properties-reader')(appRoot + '/apps/v1/dashboard/data.json')
    console.log(appRoot + '/apps/v1/dashboard/data.json')
    // return false
    let fs = require('fs')
    return new Promise(function (resolve, reject) {
        fs.readFile(appRoot + '/apps/v1/dashboard/data.json', "utf-8", (err, data) => {
            if (err) {
                console.log(err)
                resolve(err)
            }
            // console.log(data)
            resolve(data)
            // console.log(data);
        })
    })
}