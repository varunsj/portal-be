"use strict";
const appRoot = require('app-root-path');
const _ = require('underscore');

const service = require(`${appRoot}/apps/v1/dashboard/service`)
exports.dashboardData = async function (req, res, next) {
    try {
        let things = req.body
        let morePage = true;
        let limit = 100
        things['limit'] = limit
        let skip = parseInt(things._skip, 10)
        limit = limit + skip
        let res_length = 0
        things['limit'] = limit
        let val = await service.getTxtData(things)
        res_length = val.data.length
        if (res_length <= 100) {
            morePage = false
        }
        skip = skip + 100
        let ret_val = {}
        ret_val['_more_page'] = morePage
        ret_val['_skip'] = skip
        ret_val['data'] = val.data
        ret_val['total_count'] = val.total_count
        // let ex = _.find(val, function(one){
        //     return one.EmployeeID == req.body.emp_id
        // })
        // if(ex){
        //     // val = _.filter(val, function(one){
        //     //     return one.Mail == ex.Mail
        //     // })
        // }
        res.status(req.APPCONSTANTS.HTTP_STATUS.SUCCESS.CODE).send({
            "message": "Dashboard grid data",
            // "details": val,
            response : ret_val
        })
    } catch (e) {
        console.log(e)
        res.status(req.APPCONSTANTS.HTTP_STATUS.DB_ERROR.CODE).send({ error: e });
    }
}
exports.getDataPerGetRequest = async function (req, res, next) {
    try {
        let val = await service.getDataPerGetRequest(req.body)
        res.status(req.APPCONSTANTS.HTTP_STATUS.SUCCESS.CODE).send({
            "message": "Dashboard grid datasss",
            // "details": val,
            response : val
        })
    } catch (e) {
        console.log(e)
        res.status(req.APPCONSTANTS.HTTP_STATUS.DB_ERROR.CODE).send({ error: e });
    }
}

exports.authenticate = async function (req, res, next) {
    try {
        if(!req.body){
            res.status(req.APPCONSTANTS.HTTP_STATUS.DB_ERROR.CODE).send({ error: "Credentials required" });
        }
        if(!req.body.email){
            res.status(req.APPCONSTANTS.HTTP_STATUS.DB_ERROR.CODE).send({ error: "Invalid user name" });
        }
        if(!req.body.password){
            res.status(req.APPCONSTANTS.HTTP_STATUS.DB_ERROR.CODE).send({ error: "Invalid password" });
        }
        let array = await service.getAllElements(null)
        console.log(req.body.email)
        let ex = _.find(array, function(one){
            return one.EmployeeID == req.body.email.trim()
        })
        if(!ex){
            throw "Not a valid user"
        }
        res.status(req.APPCONSTANTS.HTTP_STATUS.SUCCESS.CODE).send({
            "message": "authenticated",
            "details": ex
        })
    } catch (e) {
        console.log(e)
        res.status(req.APPCONSTANTS.HTTP_STATUS.DB_ERROR.CODE).send({ error: e });
    }
}